import 'package:flutter/material.dart';
import 'package:state_management/getx/pages/getx_page.dart';
import 'package:state_management/getx2/pages/getx2_page.dart';
import 'package:state_management/provider/pages/provider_page.dart';
import 'package:state_management/provider2/pages/provider2_pages.dart';
import 'package:state_management/riverpod/pages/riverpod_page.dart';
import 'package:state_management/riverpod2/pages/riverpod2_page.dart';

void main() => runApp(const MyApp());

final navigatorKey = GlobalKey<NavigatorState>();

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'State Management',
      navigatorKey: navigatorKey,
      routes: {
        ProviderPage.route: (_) => const ProviderPage(),
        Provider2Page.route: (_) => const Provider2Page(),
        GetXPage.route: (_) => const GetXPage(),
        GetX2Page.route: (_) => const GetX2Page(),
        RiverpodPage.route: (_) => const RiverpodPage(),
        Riverpod2Page.route: (_) => const Riverpod2Page(),
      },
      home: const Home(),
    );
  }
}

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('State Management'),
        ),
        body: ListView(
          children: [
            listItem(context, 'Provider', ProviderPage.route),
            listItem(context, 'Provider2', Provider2Page.route),
            listItem(context, 'GetX', GetXPage.route),
            listItem(context, 'GetX2', GetX2Page.route),
            listItem(context, 'Riverpod', RiverpodPage.route),
            listItem(context, 'Riverpod2', Riverpod2Page.route),
          ],
        ));
  }

  ListTile listItem(BuildContext context, String title, String routeName) =>
      ListTile(
        trailing: const Icon(Icons.navigate_next),
        title: Text(title),
        onTap: () => Navigator.pushNamed(context, routeName),
      );
}
