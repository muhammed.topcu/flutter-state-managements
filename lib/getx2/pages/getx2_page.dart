import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:state_management/getx2/controller/controller2.dart';

class GetX2Page extends StatelessWidget {
  static const route = 'getx2-page';

  const GetX2Page({super.key});

  @override
  Widget build(BuildContext context) {
    return const HomeGetX2();
  }
}

class HomeGetX2 extends StatefulWidget {
  const HomeGetX2({super.key});

  @override
  State<HomeGetX2> createState() => _HomeGetX2State();
}

class _HomeGetX2State extends State<HomeGetX2> {
  late Controller2 c;

  @override
  void initState() {
    c = Get.put(Controller2());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('GetX 2 Page')),
      body: GetX<Controller2>(
        builder: (context) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Count 1: ${c.count1.value}',
                  style: const TextStyle(fontSize: 24),
                ),
                const SizedBox(height: 16),
                Text(
                  'Count 2: ${c.count2.value}',
                  style: const TextStyle(fontSize: 24),
                ),
              ],
            ),
          );
        },
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: () => c.incrementCount1(),
            child: const Icon(Icons.looks_one),
          ),
          const SizedBox(height: 16),
          FloatingActionButton(
            onPressed: () => c.incrementCount2(),
            child: const Icon(Icons.looks_two),
          ),
        ],
      ),
    );
  }
}
