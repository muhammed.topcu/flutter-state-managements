import 'package:get/get.dart';

class Controller2 extends GetxController {
  var count1 = 0.obs;
  var count2 = 0.obs;

  void incrementCount1() {
    count1.value++;
  }

  void incrementCount2() {
    count2.value++;
  }
}
