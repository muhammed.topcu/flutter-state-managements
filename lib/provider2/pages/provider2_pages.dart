import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_management/provider2/providers/data_provider2.dart';

class Provider2Page extends StatelessWidget {
  static const route = 'provider2-page';

  const Provider2Page({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => DataProvider2(),
      child: const HomeProvider(),
    );
  }
}

class HomeProvider extends StatefulWidget {
  const HomeProvider({super.key});

  @override
  State<HomeProvider> createState() => _HomeProviderState();
}

class _HomeProviderState extends State<HomeProvider> {
  late DataProvider2 provider;

  @override
  void initState() {
    provider = Provider.of<DataProvider2>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Provider 2 Page')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Consumer<DataProvider2>(
              builder: (context, counter, child) {
                return Text(
                  'Count 1: ${counter.count1}',
                  style: const TextStyle(fontSize: 24),
                );
              },
            ),
            const SizedBox(height: 16),
            Consumer<DataProvider2>(
              builder: (context, counter, child) {
                return Text(
                  'Count 2: ${counter.count2}',
                  style: const TextStyle(fontSize: 24),
                );
              },
            ),
          ],
        ),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: () => provider.incrementCount1(),
            child: const Icon(Icons.looks_one),
          ),
          const SizedBox(height: 16),
          FloatingActionButton(
            onPressed: () => provider.incrementCount2(),
            child: const Icon(Icons.looks_two),
          ),
        ],
      ),
    );
  }
}
