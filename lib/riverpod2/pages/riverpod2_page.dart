import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:state_management/riverpod2/provider/riverpod2_provider.dart';

class Riverpod2Page extends StatelessWidget {
  static const route = 'riverpod2-page';

  const Riverpod2Page({super.key});

  @override
  Widget build(BuildContext context) {
    return const ProviderScope(
      child: HomeRiverpod(),
    );
  }
}

class HomeRiverpod extends ConsumerWidget {
  const HomeRiverpod({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final data = ref.read(riverpod2Provider.notifier);

    return Scaffold(
      appBar: AppBar(title: const Text('Riverpod Page')),
      //It is best practice to put your Consumer widgets as deep in the tree as possible.
      //You don’t want to rebuild large portions of the UI just because some detail somewhere changed.
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Consumer(
              builder: (context, watch, child) {
                final counter = ref.watch(riverpod2Provider);
                return Text(
                  'Count 1: ${counter.count1}',
                  style: const TextStyle(fontSize: 24),
                );
              },
            ),
            const SizedBox(height: 16),
            Consumer(
              builder: (context, watch, child) {
                final counter = ref.watch(riverpod2Provider);
                return Text(
                  'Count 2: ${counter.count2}',
                  style: const TextStyle(fontSize: 24),
                );
              },
            ),
          ],
        ),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: () => data.incrementCount1(),
            child: const Icon(Icons.looks_one),
          ),
          const SizedBox(height: 16),
          FloatingActionButton(
            onPressed: () => data.incrementCount2(),
            child: const Icon(Icons.looks_two),
          ),
        ],
      ),
    );
  }
}
